               OpCapability Shader
               OpCapability VariablePointers
               OpExtension "SPV_KHR_variable_pointers"
          %1 = OpExtInstImport "GLSL.std.450"
               OpMemoryModel Logical GLSL450
               OpEntryPoint GLCompute %main "main"
               OpExecutionMode %main LocalSize 1 1 1
               OpSource GLSL 430
               OpMemberDecorate %Indices 0 Offset 0
               OpMemberDecorate %Indices 1 Offset 4
               OpMemberDecorate %Indices 2 Offset 8
               OpDecorate %Indices Block
               OpMemberDecorate %Result 0 Offset 0
               OpDecorate %Result Block
               OpDecorate %result DescriptorSet 0
               OpDecorate %result Binding 2
               OpDecorate %_arr_float_int_10 ArrayStride 4
               OpMemberDecorate %InputData 0 Offset 0
               OpDecorate %InputData Block
               OpDecorate %input0 DescriptorSet 0
               OpDecorate %input0 Binding 0
               OpDecorate %input1 DescriptorSet 0
               OpDecorate %input1 Binding 1
               OpDecorate %texSampler DescriptorSet 0
               OpDecorate %texSampler Binding 0
       %void = OpTypeVoid
       %bool = OpTypeBool
        %int = OpTypeInt 32 1
      %float = OpTypeFloat 32
  %void_func = OpTypeFunction %void
    %Indices = OpTypeStruct %int %int %int
%_ptr_PushConstant_Indices = OpTypePointer PushConstant %Indices
    %indices = OpVariable %_ptr_PushConstant_Indices PushConstant
      %int_0 = OpConstant %int 0
      %int_1 = OpConstant %int 1
      %int_2 = OpConstant %int 2
      %int_3 = OpConstant %int 3
     %int_10 = OpConstant %int 10
    %v2float = OpTypeVector %float 2
    %v4float = OpTypeVector %float 4
%const_float = OpConstant %float 0
%_ptr_PushConstant_int = OpTypePointer PushConstant %int
     %Result = OpTypeStruct %float
%_ptr_StorageBuffer_Result = OpTypePointer StorageBuffer %Result
     %result = OpVariable %_ptr_StorageBuffer_Result StorageBuffer
%_arr_float_int_10 = OpTypeArray %float %int_10
  %InputData = OpTypeStruct %_arr_float_int_10
%_arr_InputData_int_3 = OpTypeArray %InputData %int_3
%_ptr_StorageBuffer__arr_InputData_int_2 = OpTypePointer StorageBuffer %_arr_InputData_int_3
%_ptr_StorageBuffer_int = OpTypePointer StorageBuffer %float
     %input0 = OpVariable %_ptr_StorageBuffer__arr_InputData_int_2 StorageBuffer
     %input1 = OpVariable %_ptr_StorageBuffer__arr_InputData_int_2 StorageBuffer
         %59 = OpTypeImage %float 2D 0 0 0 1 Unknown
         %60 = OpTypeSampledImage %59
%_ptr_UniformConstant_60 = OpTypePointer UniformConstant %60
 %texSampler = OpVariable %_ptr_UniformConstant_60 UniformConstant
       %main = OpFunction %void None %void_func
      %label = OpLabel
   %idx0_ptr = OpAccessChain %_ptr_PushConstant_int %indices %int_0
       %idx0 = OpLoad %int %idx0_ptr
   %idx1_ptr = OpAccessChain %_ptr_PushConstant_int %indices %int_1
       %idx1 = OpLoad %int %idx1_ptr
   %idx2_ptr = OpAccessChain %_ptr_PushConstant_int %indices %int_2
       %idx2 = OpLoad %int %idx2_ptr
  %idx0_zero = OpIEqual %bool %idx0 %int_0
%variable_ptr = OpSelect %_ptr_StorageBuffer__arr_InputData_int_2 %idx0_zero %input0 %input1
%input_data_ptr = OpAccessChain %_ptr_StorageBuffer_int %variable_ptr %idx1 %int_0 %idx2
 %input_data = OpLoad %float %input_data_ptr
%result_data_ptr = OpAccessChain %_ptr_StorageBuffer_int %result %int_0
               OpStore %result_data_ptr %input_data
               OpReturn
               OpFunctionEnd
