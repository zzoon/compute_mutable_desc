VULKAN_SDK_PATH = /data/vulkan/sdk/aarch64
CFLAGS = -g -std=c++17 -I$(VULKAN_SDK_PATH)/include
LDFLAGS = -L$(VULKAN_SDK_PATH)/lib -lvulkan
VK_LAYER_PATH="$(VULKAN_SDK_PATH)/etc/vulkan/explicit_layer.d"

Compute: main.cpp
	g++ $(CFLAGS) -o Compute main.cpp $(LDFLAGS)

.PHONY: test clean

test: Compute
	LD_LIBRARY_PATH=$(VULKAN_SDK_PATH)/lib:$$LD_LIBRARY_PATH VK_LAYER_PATH=$(VK_LAYER_PATH) ./Compute

clean:
	rm -f Compute
