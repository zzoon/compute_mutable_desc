# compute_mutable_desc

Test code for mutable descritors.

# Shader

          #version 430

          layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

          layout(binding = 2) buffer _4_5
          {
              float _m0;
          } RES;

          layout(binding = 0) buffer _7_8
          {
              float _m0[10];
          } A[3];

          layout(binding = 1) buffer _7_9
          {
              float _m0[10];
          } B[3];

          layout(binding = 0) uniform sampler2D tex[3];

          layout(push_constant) uniform pcblock { int _m0; int _m1; int _m2; } pc;

          void main()
          {
              RES._m0 = ((pc._m0 == 0) ? &A : &B)[pc._m1]._m0[pc._m2];
          }

# spirv-as
spirv-as --target-env vulkan1.1spv1.3 shader.txt -o comp.spv
